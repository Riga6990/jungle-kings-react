import React, {Component} from 'react';
import SignUp from '../components/SignIn';
import {schedule} from '../mapper/schedule/schedule';
import { Link } from "react-router-dom";

class Schedule extends Component {
	hasColor(color) {
		return color ? color : '';
	};
	isGroup(group) {
		if (group) {
			return group.map(key => (<p key={key}>{key}</p>))
		} else {
			return (<br />)
		}
	};
	training(data) {
		if (Object.keys(data).length) {
		    return (
		    	<>
					<Link to={`/styles/${data.style}`}>{data.dance}</Link>
					{this.isGroup(data.group)}
					<Link to={`/teachers/${data.id}`}>{data.name}</Link>
				</>
			)
		}
	}
	tableMobile() {
		const byHours = (list) => {
			return list.map(key => {
				return (
					<React.Fragment key={key.hour}>
						<div className="schedule-table-mobile--hour">{key.hour}</div>
						<div className="schedule-table-mobile--training">
							<div style={{background: this.hasColor(key.one_hall.color)}}>
								{this.training(key.one_hall)}
							</div>
							<div style={{background: this.hasColor(key.two_hall.color)}}>
								{this.training(key.two_hall)}
							</div>
						</div>
					</React.Fragment>
				)
			})
		};
		return schedule.map((key, index) => {
			return (
				<div className="schedule-table-mobile" key={index}>
					<div className="schedule-table-mobile--time">Время</div>
					<div className="schedule-table-mobile--day">{key.day}</div>
					<div className="schedule-table-mobile--hall">Зал</div>
					<div className="schedule-table-mobile--halls">
						<div>Зал 1</div>
						<div>Зал 2</div>
					</div>
					{byHours(key.hours)}
				</div>
			)
		})
	}

	tableDesktop() {
		const byHours = (time) => time.map((key, index) => (<div key={index} className="schedule-table-desktop-hours--hour">{key.hour}</div>));
		const byRows = (rows) => {
			return rows.map((key, index) => {
				return (
					<div key={index} className="schedule-table-desktop-columns-column--training">
						<div className="schedule-table-desktop-columns-column--training-row"  style={{background: this.hasColor(key.one_hall.color)}}>
							<div>
								{this.training(key.one_hall)}
							</div>
						</div>
						<div className="schedule-table-desktop-columns-column--training-row" style={{background: this.hasColor(key.two_hall.color)}}>
							<div>
								{this.training(key.two_hall)}
							</div>
						</div>
					</div>
				)
			})
		};
		const byColumns = (list) => {
			return list.map((key, index) => {
				return (
					<div key={index} className="schedule-table-desktop-columns-column">
						<div className="schedule-table-desktop-columns-column--day">{key.day}</div>
						<div className="schedule-table-desktop-columns-column--halls">
							<div>Зал 1</div>
							<div>Зал 2</div>
						</div>
						{byRows(key.hours)}
					</div>
				)
			})
		};
		return (
			<div className="schedule-table-desktop">
				<div className="schedule-table-desktop-hours">
					<div className="schedule-table-desktop-hours--time">Время</div>
					<div className="schedule-table-desktop-hours--hall">Зал</div>
					{byHours(schedule[0].hours)}
				</div>
				<div className="schedule-table-desktop-columns">{byColumns(schedule)}</div>
			</div>
		)
	}

	render() {
		return (
			<main className="content-page schedule">
				<section className="schedule-title">
					<h2>Расписание <i>JUNGLE KINGS</i></h2>
					<SignUp eventEmit={this.props.handlerModal} />
				</section>
				<section className="schedule-table">
					{this.props.sizePage  === 'desktop' ? this.tableDesktop() : this.props.sizePage  === 'laptop' ? this.tableDesktop() : this.tableMobile()}
				</section>
			</main>
		)
	}
}

export default Schedule
