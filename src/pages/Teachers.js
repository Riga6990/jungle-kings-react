import React, {Component} from 'react';
import {teachers} from "../mapper/teachers/teachers";
import { Link } from "react-router-dom";
import SignUp from '../components/SignIn';
import vk from '../static/images/icons/vk.png'
import ig from '../static/images/icons/ig.png'

class Teachers extends Component {
	constructor() {
		super();
		this.state = {
			number: 0
		}
	}
	filterTeacher(info) {
		const teacher = this.props.props.location.pathname.replace('/teachers/', '');
		info = info.find(key => key.teacher_id === teacher);
		return info;
	}
	information(teacher) {
		if (teacher) {
			const howYear = (n) => {
				const plural = (n % 10 === 1 && n % 100 !== 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2);
				switch (plural) {
					case 0: return `${n} год`;
					case 1: return `${n} года`;
					case 2: return `${n} лет`;
					default: return 'передан неверный формат'
				}
			};
			const experience = (date) => {
				if (date) {
					let current = new Date().getFullYear() - date;
					return `${howYear(current)}`
				} else {
					return 'В ближайшее время мы добавим информацию.'
				}
			};
			const isDance = (dance) => dance.map((key, i) => (<Link key={i} to={`/styles/${key}`}>{ key.toUpperCase() }</Link>));
			const detailsForm = (text) => text.map((key, i) => (<p key={i}>{ key }</p>));
			return (
				<div key={teacher.teacher_id} className="teachers-section-achievements-block">
					<div>
						<span>{teacher.info.name}</span>
					</div>
					<div>
						<span>Основные направления:</span>
						{isDance(teacher.info.directions)}
					</div>
					<div>
						<span>Танцевальный стаж:</span>
						{experience(teacher.info.experience)}</div>
					<div>
						<span>Преподавательскый стаж:</span>
						{experience(teacher.info.mentor)}</div>
					<div>
						{detailsForm(teacher.info.about_self)}
					</div>
					<div>
						<span>Достижения:</span>
						{detailsForm(teacher.info.personal_achievements)}
					</div>
					<div>
						<span>Мастер-классы:</span>
						{detailsForm(teacher.info.master_class)}
					</div>
				</div>
			)
		} else {
			return (<div>Кликни на имя преподавателя, чтобы узнать о нем больше!</div>)
		}
	};
	profile(teacher) {
		if (teacher) {
			const forEachPhoto = (step) => {
				const len = teacher.info.social_network.photos.length;
				const num = this.state.number;
				if (step === 'previous') {
					if (num === 0) {
						this.setState((state) => state.number = len - 1)
					} else {
						this.setState((state) => state.number = state.number - 1)
					}
				} else if (step === 'next') {
					if (num === len - 1) {
						this.setState((state) => state.number = 0)
					} else {
						this.setState((state) => state.number = state.number + 1)
					}
				} else {
				    alert('Ошибка ввода данных');
				}
			}
			return(
				<div className="teachers-section-gallery--rhombus">
					<div className="teachers-section-gallery--rhombus-vk-photo">
						<img src={teacher.info.social_network.vk.photo} alt={teacher.info.name + 'в контакте'} />
					</div>
					<a href={teacher.info.social_network.vk.link} target="_blank" rel="noopener noreferrer" className="teachers-section-gallery--rhombus-vk-link">
						<span>
							<img src={vk} alt={`Профиль ${teacher.info.name} в контакте`} />
						</span>
					</a>
					<div className="teachers-section-gallery--rhombus-ig-photo">
						<img src={teacher.info.social_network.ig.photo} alt={teacher.info.name + 'в инстаграмме'} />
					</div>
					<a href={teacher.info.social_network.ig.link} target="_blank" rel="noopener noreferrer" className="teachers-section-gallery--rhombus-ig-link">
						<span>
							<img src={ig} alt={`Профиль ${teacher.info.name} в инстаграмме`} />
						</span>
					</a>
					<div className="teachers-section-gallery--rhombus-all">
						<img src={teacher.info.social_network.photos[this.state.number]} alt={`Фотографии преподавателя ${teacher.info.name}`}/>
					</div>
					<div className="teachers-section-gallery--rhombus-previous" onClick={ () => forEachPhoto('previous') } >↑</div>
					<div className="teachers-section-gallery--rhombus-next" onClick={ () => forEachPhoto('next') }>↓</div>
				</div>
			)
		}
	}
	listTeachers(list) {
		const teacher = this.props.props.location.pathname.replace('/teachers/', '');
		return list.map(key => {
			return (
				<Link key={key.teacher_id}
					  to={`/teachers/${key.teacher_id}`}
					  className={'teachers-section-list--teacher' + (key.teacher_id === teacher ? ' active' : '')}
				>
					{key.info.name} {key.teacher_id === teacher ? '→' : ''}
				</Link>
			)
		})
	}
	render() {
		return (
			<main className="content-page teachers">
				<section className="teachers-title">
					<h2>
						Преподаватели <i>Jungle Kings</i>
					</h2>
					<p>
						Самые яркие преподаватели современных танцев в Твери здесь! У каждого из них многолетний опыт занятий танцами
						и преподавания, победы на соревнованиях и свой подход к ученикам.
					</p>
				</section>
				<section className="teachers-section">
					<section className="teachers-section-list">
						{this.listTeachers.call(this, teachers)}
						<SignUp eventEmit={this.props.handlerModal} />
					</section>
					<section className="teachers-section-gallery">
						{this.profile(this.filterTeacher.call(this, teachers))}
					</section>
					<section className="teachers-section-achievements">
						{this.information(this.filterTeacher.call(this, teachers))}
					</section>
				</section>
			</main>
		)
	}
}

export default Teachers

