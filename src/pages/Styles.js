import React, {Component} from 'react';
import SignUp from '../components/SignIn';
import {dance} from '../mapper/classes'
import { Link } from "react-router-dom";

class Styles extends Component {
	scrollToBottom(id) {
        const el = document.getElementById(id);
        if (el !== null) {
			el.scrollIntoView({ behavior: "smooth" })
        }
    }
    listStyles(list, path) {
        return list.map(key => {
            return(
            <Link key={key.id} to={`/styles/${key.id}`} className={"dance-content-styles--rhombus" + (key.id === path ? ' active' : '') } id={`rhombus-${key.id}`} >
                <span>{key.title}</span>
            </Link>
            )
        })
    }
    blockInfo(list, path) {
        const tag = list.find(key => key.id === path);
        const editorText = (text) => {
			text = text.split('\n\n');
			let paragraph = [];
			text.forEach((key, i) => paragraph.push(<p key={i}>{key}</p>));
			return paragraph
        };
        const hasDescription = (text) => {
            if (text !== undefined) {
                return (
                    <div className="dance-content-description--block-content" id={'description-' + text.id}>
                        <h3>{text.title}</h3>
                        <article>{editorText(text.description)}</article>
                    </div>
                )
            } else {
                return (
                    <div>
                        <h3>Кликни на ромб, чтобы узнать о стиле больше</h3>
                    </div>
                )
            }
        };
        return (
            <div className="dance-content-description--block">
                {hasDescription(tag)}
                <SignUp eventEmit={this.props.handlerModal} />
            </div>
        )
    }
    hasPathname() {
	    return this.props.props.location.pathname.replace('/styles/', '');
    }
	componentDidMount() {
		this.scrollToBottom('description-' + this.hasPathname())
	}
	componentDidUpdate() {
		this.scrollToBottom('description-' + this.hasPathname())
    }
    render() {
        const pathName = this.hasPathname();
        return (
            <main className="content-page dance">
                <section className="dance-title">
                    <h2>Стили</h2>
                </section>
                <section className="dance-content">
                    <section className="dance-content-styles">
                        {this.listStyles.call(this, dance, pathName)}
                    </section>
                    <section className="dance-content-description">{this.blockInfo.call(this, dance, pathName)}</section>
                </section>
            </main>
        )
    }
}

export default Styles
