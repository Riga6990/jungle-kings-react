import React, {Component} from 'react';
import SignUp from '../components/SignIn'
import {soc_network} from '../mapper/soc_network'
import {withRouter} from "react-router-dom";

class Main extends Component {
	socialNetwork (links) {
		const list = links.map((key) => {
			return (
			<a key={key.id} target='_blank' href={key.link} rel="noopener noreferrer">
				<li id={key.id}>
					<img src={key.img} alt={key.alt} />
				</li>
			</a>
			)
		});
		return (<ul className="main-page-social-network">{list}</ul>)
	}
	render() {
		return (
			<main className="content-page main-page main_container">
				<section>
					<h1>
						<i>Студия танца</i>
						<br/>
						Jungle Kings
					</h1>
					<article className="main-page-description">
						Seamlessly visualize frictionless catalysts for change before compelling initiatives. Phosfluorescently whiteboard multidisciplinary human capital via team driven mindshare.
					</article>
					<SignUp eventEmit={this.props.handlerModal} />
					{this.socialNetwork(soc_network)}
                </section>
			</main>
		)
	}
}

export default withRouter(Main)
