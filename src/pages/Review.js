import React, {Component} from 'react';
import SignUp from '../components/SignIn';
import {review} from '../mapper/list_review';

class Review extends Component {
	constructor() {
		super();
		this.state = {
			load: 0
		}
	}
	getText (text) {
		text = text.split('\n');
		let tag = [];
		text.forEach((key, i) => tag.push(<article key={i}>{key}</article>));
		return tag
	}
	listReview() {
		const listItem = (list) => {
			return list.map(key => {
				return (
					<div className="review-content-block" key={key.id}>
						<div className="review-content-block--rhombus">
							<img src={key.photo} alt={key.fullName} />
						</div>
						<div className="review-content-block--information">
							<div className="review-content-block--information-name">{key.fullName}</div>
							<div className="review-content-block--information-text">{this.getText(key.text)}</div>
							<div className="review-content-block--information-about" onClick={() => this.setState({load: key.id})}>Читать далее</div>
						</div>
					</div>
				)
			})
		};
		return (
			<section className="review-content">
				{listItem(review)}
			</section>
		)
	}
	readMore() {
		const member = review.find(key => key.id === this.state.load);
		const reviewMember = (description) => {
			return (
				<div className="review-member-block" key={description.id}>
					<div className="review-member-block--rhombus">
						<img src={description.photo} alt={description.fullName} />
					</div>
					<div className="review-member-block--information">
						<div className="review-member-block--information-name">{description.fullName}</div>
						<div className="review-member-block--information-text">{this.getText(description.text)}</div>
					</div>
					<div className="review-member-block--information-about" onClick={() => this.setState({load: 0})}>Вернуться назад</div>
				</div>
			)
		};
		return(
			<section className="review-member">
				{reviewMember(member)}
			</section>
		)
	}
	render() {
		return (
			<main className="content-page review">
				<section className="review-title">
					<h2>Отзывы</h2>
					<SignUp eventEmit={this.props.handlerModal} />
				</section>
				{this.state.load === 0 ? this.listReview() : this.readMore()}
			</main>
		)
	}
}

export default Review
