import React, {Component} from 'react';
import { live } from '../mapper/live/live'
import { Link } from "react-router-dom";
import SignUp from "../components/SignIn";

class Live extends Component {
	blockOfSection(props) {
		const hasLogo = (image, id) => {
			if (image) {
			    return (
			    	<div className={'bg-' + id} />
				)
			}
		};
		const rhombus = (list) => {
			return list.map(key => {
				if (key.img) {
				    return(
						<Link to={key.id === 'review' ? '/review' : `/live/${key.id}`} key={key.id} className={`live-jk-section--rhombus ${key.id}`}>
							<span>
								{hasLogo(key.img, key.id)}
								<div>{key.text}</div>
							</span>
						</Link>
					)
				} else {
					return (
						<div key={key.id} className={`live-jk-section--rhombus ${key.id}`}>
							<span>{key.text}</span>
						</div>
					)
				}
			})
		};
		return(
			<section className="live-jk-section">
				{rhombus(props)}
			</section>
		)
	}
	liveJk(live) {
		return (
			<main className="content-page live-jk">
				<section className="live-jk-title">
					<h2>Жизнь <i>Jungle Kings</i></h2>
					<SignUp eventEmit={this.props.handlerModal} />
				</section>
				{this.blockOfSection(live)}
			</main>
		)
	}
	render() {
		return (
			<>
				{this.props.props.location.pathname === '/live' ? this.liveJk.call(this, live) : ''}
			</>
		)
	}
}

export default Live
