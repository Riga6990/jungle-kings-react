import React, {Component} from 'react';
import {Link} from "react-router-dom";
import LogoBlack from "../static/images/logo-black.png";

class NotFound extends Component {
	render () {
		return (
			<div id='not-found'>
				<div>
					<Link to={'/main'}><img src={LogoBlack} alt='Логотип Jungle Kings'/></Link>
					<Link to={'/main'}>Вернуться на главную страницу</Link>
				</div>
			</div>
		)
	}
}

export default NotFound;
