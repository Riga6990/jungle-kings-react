import React, {Component} from 'react';
import SignUp from '../components/SignIn'
import phone from '../static/images/phone.png'

class Contacts extends Component {
	render() {
		return (
			<main className="content-page contacts">
				<section className="contacts-title">
					<h2>КОНТАКТЫ</h2>
					<SignUp eventEmit={this.props.handlerModal}/>
				</section>
				<section className="contacts-rhombus">
					<div className="contacts-rhombus-info">
						<span>
							<img src={phone} alt="Phone Jungle Kings"/>
							<div>Записаться на пробное занятие <br/> можно совершенно <b>бесплатно!</b></div>
						</span>
					</div>
					<div className="contacts-rhombus-phone">
						<span>Телефон в Твери: <br/> 60-09-70</span>
					</div>
					<div className="contacts-rhombus-address">
						<span>ул. Семионовская, <br/>дом 74</span>
					</div>
					<div className="contacts-rhombus-online" onClick={this.props.handlerModal}>
						<span>Записаться <br/> онлайн</span>
					</div>
				</section>
			</main>
		)
	}
}

export default Contacts
