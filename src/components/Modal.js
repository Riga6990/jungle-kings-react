import React, {Component} from 'react';
import Sign from './SignIn'

class Modal extends Component {
    sendData () {
        alert()
    }
    fieldOpacity (e) {
        if (e.target.id === 'modal') {
            this.props.handlerModal()
        }
    }
    render () {
        return (
            <div id="modal" className="wrapper" onClick={this.fieldOpacity.bind(this)}>
                <div className="modal">
                    <div className="modal-close" onClick={this.props.handlerModal}>
                        <span>X</span>
                    </div>
                    <div className="modal-form">
                        <span className="modal-form-text">Заполни форму <br/> и мы пригласим тебя на бесплатное занятие!</span>
                        <form action="">
                            <input type="text" className="modal-form-input" placeholder="ФИО"/>
                            <input type="text" className="modal-form-input" placeholder="Телефон"/>
                            <input type="text" className="modal-form-input" placeholder="Email"/>
                        </form>
                        <Sign eventEmit={this.sendData} />
                    </div>
                </div>
            </div>
        )
    }
}

export default Modal

