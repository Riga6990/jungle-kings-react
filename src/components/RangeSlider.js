import React, {Component} from 'react'
import {withRouter} from "react-router-dom";

class RangeSlider extends Component {
	getCoordinates(elem) {
		const box = elem.getBoundingClientRect();
		return box.left + window.pageXOffset
	}
	sizeComponent(laptop, desktop) {
		switch (this.props.sizePage) {
			case 'laptop':
				return laptop + 'px';
			case 'desktop':
				return desktop + 'px';
			default:
				break;
		}
	}
	getItem (coordinate) {
		coordinate = Number(coordinate.replace('px', ''));
		/**
		 * При версии desktop используется начальная координата -8px и последняя 1140px
		 * растояние между точками 191.33px при расширение более 1200px экрана
		 * (1140 - -8) / 6 = 191.33
		 *  растояние между точками 163.66px при расширение от 1024px до 1199px экрана
		 * (964 - -8) / 6 = 162
		 * соотвественно далее аргумент coordinate будет делиться на данную точку
		 */
		let size = Number(this.sizeComponent(162, 191.33).replace('px', '')),
			result = coordinate / size;
		return Math.round(result)
	}
	scrollInSide(e) {
		const sliderElem = document.getElementById('footer-menu'), // поле, в котором находится ромбик
			ranger = document.getElementById('ranger'), // сам ромбик
			rhombusCoordinates = this.getCoordinates(ranger),
			shiftX = e.pageX - rhombusCoordinates,
			sliderCoordinates = this.getCoordinates(sliderElem);

		document.onmousemove = function(e) {
			//  вычесть координату родителя, т.к. position: relative

			let isLeft = e.pageX - shiftX - sliderCoordinates;

			// проверка на то, чтобы курсор не ушел за пределы слева

			if (isLeft < -8) {
				isLeft = -8;
			}

			// проверка на то, чтобы курсор не ушел за пределы справа

			const rightEdge = sliderElem.offsetWidth - ranger.offsetWidth;
			if (isLeft > rightEdge - -7) {
				isLeft = rightEdge - -7;
			}

			ranger.style.left = isLeft + 'px';
		};

		let vm = this;

		document.onmouseup = function() {
			let pointX = vm.getItem(ranger.style.left);
			switch (pointX) {
				case 0:
					ranger.style.left = '-8px';
					vm.props.history.push('/main');
					break;
				case 1:
					ranger.style.left = vm.sizeComponent(154, 183);
					vm.props.history.push('/live');
					break;
				case 2:
					ranger.style.left = vm.sizeComponent(317, 375);
					vm.props.history.push('/styles');
					break;
				case 3:
					ranger.style.left = vm.sizeComponent(479, 566);
					vm.props.history.push('/teachers');
					break;
				case 4:
					ranger.style.left = vm.sizeComponent(641, 758);
					vm.props.history.push('/review');
					break;
				case 5:
					ranger.style.left = vm.sizeComponent(803, 950);
					vm.props.history.push('/schedule');
					break;
				case 6:
					ranger.style.left = vm.sizeComponent(964, 1140);
					vm.props.history.push('/contacts');
					break;
				default:
					break;
			}
			document.onmousemove = document.onmouseup = null;
		};

		return false; // disable selection start (cursor change)
	}
	componentDidUpdate() {
		/**
		 * Определяем месторасположения ромбика в момент загрузки/перезагрузки страницы и подставляем в нужную точку
		 */
		setTimeout(() => {
			const ranger = document.getElementById('ranger'),
				location = this.props.location.pathname.split('/')[1];
			if (this.props.sizePage === 'desktop' || this.props.sizePage === 'laptop') {
				switch (location) {
					case 'main':
						ranger.style.left = '-8px';
						break;
					case 'live':
						ranger.style.left = this.sizeComponent(154, 183);
						break;
					case 'styles':
						ranger.style.left = this.sizeComponent(317, 375);
						break;
					case 'teachers':
						ranger.style.left = this.sizeComponent(479, 566);
						break;
					case 'review':
						ranger.style.left = this.sizeComponent(641, 758);
						break;
					case 'schedule':
						ranger.style.left = this.sizeComponent(803, 950);
						break;
					case 'contacts':
						ranger.style.left = this.sizeComponent(964, 1140);
						break;
					default:
						break;
				}
			}
		}, 100)
	}
	slider() {
		const listItems = item => item.map((key) => <li key={key}/>);
		return (
			<nav className="footer-nav">
				<ul className={"footer-nav-scroll"} id="footer-menu">{listItems(this.props.links)}</ul>
				<div id="ranger" className="footer-nav-ranger" onMouseDown={this.scrollInSide.bind(this)} />
			</nav>
		)
	}

	render() {
		return (
			<footer className={"footer " + (this.props.isColor)}>
				{this.props.sizePage === 'desktop' ? this.slider() : this.props.sizePage === 'laptop' ? this.slider() : <></>}
				<div className="footer-content"/>
			</footer>
		);
	}
}

export default withRouter(RangeSlider)
