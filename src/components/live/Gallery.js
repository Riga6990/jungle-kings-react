import React, {Component} from 'react';
import {Link} from "react-router-dom";

const data = [
	{name: 'Фото', link: '/live/gallery/photo', isClasses: 'photo'},
	{name: 'Видео', link: '/live/gallery/video', isClasses: 'video'},
	{name: 'Вернуться назад', link: '/live', isClasses: 'back'}
];

class Gallery extends Component {
	photoOrVideo(list) {
		return list.map(key => {
			return (
				<Link to={key.link} key={key.isClasses} className={'live-jk-gallery-rhombus photo ' + (key.isClasses)}>
					<span>{key.name}</span>
				</Link>
			)
		})
	}

	galleryJk() {
		return (
			<main className="content-page live-jk">
				<section className="live-jk-title">
					<h2>Галерея <i>Jungle Kings</i></h2>
				</section>
				<section className="live-jk-gallery">
					{this.photoOrVideo(data)}
				</section>
			</main>
		)
	}

	render() {
		return (
			<>
				{this.props.location.pathname === '/live/gallery' ? this.galleryJk() : <></>}
			</>
		)
	}
}

export default Gallery;
