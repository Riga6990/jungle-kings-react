import React, {Component} from 'react';
import { videos } from '../../mapper/live/films'
import BackStep from "./BackStep";
import Loader from '../../components/Loader'

class Films extends Component {
	constructor() {
		super();
		this.state = {
			display: 'none'
		}
	}
    films(v) {
        return v.map((key, index) => {
            return (
				<iframe
                    width="320"
                    height="165"
                    src={key}
                    frameBorder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen
                    key={index}
					title={key}
                >
                </iframe>
            )
        })
    }
	componentDidMount() {
		setTimeout(
			function() {
				this.setState({display: 'flex'});
			}.bind(this), 15000
		);
	}
	informationAboutLoad() {
		if (this.state.display === 'none') {
		    return (<section className="live-jk-information">
				<div>
					<p>Подождите, идет загрузка видео</p>
					<Loader />
				</div>
			</section>)
		}
	}

    render() {
        return (
			<main className="content-page live-jk">
				<section className="live-jk-title">
					<h2>Видео с <i>Jungle Kings</i></h2>
					<BackStep step="/live/gallery" />
				</section>
				{this.informationAboutLoad()}
                <section className="live-jk-videos" style={{display: this.state.display}}>
                    {this.films(videos)}
                </section>
			</main>
        )
    }
}

export default Films
