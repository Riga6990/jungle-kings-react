import React, {Component} from 'react';
import { awards } from '../../mapper/live/awards'
import BackStep from "./BackStep";

class Awards extends Component {
	listAwards(list) {
		const isEvents = (list) => {
		    return list.map((event, index) => {
				return(
					<article key={index} className="live-jk-awards-blocks-block--event">{event}</article>
				)
            })
		};
	    const itDescription = (props) => {
	        return props.map(key => {
				return (
					<div key={key.year} className="live-jk-awards-blocks-block">
						<p className="live-jk-awards-blocks-block--year">{key.year}</p>
                        {isEvents(key.events)}
						<br/>
					</div>
				)
			})
        };
	    return list.map(key => {
	        return (
	            <div key={key.direction}  className="live-jk-awards-blocks">
                    <h4>{key.direction}</h4>
                    {itDescription(key.details)}
                </div>
            )
        })
    }
    render() {
        return (
        	<main className="content-page live-jk">
				<section className="live-jk-title">
					<h2>Достижения <i>Jungle Kings</i></h2>
					<BackStep step="/live" />
				</section>
				<section className="live-jk-awards">
					{this.listAwards(awards)}
				</section>
			</main>
        )
    }
}

export default Awards
