import React, {Component} from 'react';
import {images} from '../../mapper/live/photos'
import BackStep from "./BackStep";

class Photography extends Component {
	constructor() {
		super();
		this.state = {
			photo: ''
		}
	}

	gallery(list) {
		return list.map(photo => {
			return (
				<div key={photo} style={{backgroundImage: `url(${photo})`}}
					 className="live-jk-photos--photo"
					 onClick={() => this.setState({ photo: photo })}
				>
				</div>
			)
		})
	}

	numberPhoto(photos) {
		return photos.findIndex(key => key === this.state.photo) + 1;
	}

	selectPhoto(list, step) {
		const current = this.numberPhoto(list) - 1;
		if (step === 'previous') {
			if (current === 0) {
				this.setState({ photo: list[list.length - 1] });
			} else {
				this.setState({ photo: list[current - 1] });
			}
		}
		if (step === 'next') {
			if (current === list.length - 1) {
				this.setState({ photo: list[0] });
			} else {
				this.setState({ photo: list[current + 1] });
			}
		}
	}

	showLargePhoto(images) {
		if (this.state.photo) {
		    return (
				<section className="wrapper live-jk-photos-section">
					<div className="live-jk-photos-section--large-photo">
						<img src={this.state.photo} alt="Жизнь в Jungle Kings" />
						<div>{this.numberPhoto(images)} из {images.length}</div>
					</div>
					<div className="live-jk-photos-section--close" onClick={ () => this.setState({ photo: '' }) }>X</div>
					<div className="live-jk-photos-section--prev" onClick={ () => this.selectPhoto(images, 'previous') }>❮</div>
					<div className="live-jk-photos-section--next" onClick={ () => this.selectPhoto(images, 'next') }>❯</div>
				</section>
			)
		}
	}

	render() {
		return (
			<main className="content-page live-jk">
				<section className="live-jk-title">
					<h2>Фотоотчет <i>Jungle Kings</i></h2>
					<BackStep step="/live/gallery" />
				</section>
				<section className="live-jk-photos">
					{this.gallery(images)}
					{this.showLargePhoto.call(this, images)}
				</section>
			</main>
		)
	}
}

export default Photography
