import React, {Component} from 'react';
import { Link } from "react-router-dom";

class BackStep extends Component {
	render() {
		return(
			<Link to={this.props.step} className="sign-up">
				Вернуться назад
			</Link>
		)
	}
}

export default BackStep
