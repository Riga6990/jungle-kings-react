import React, {Component} from 'react';
import LogoBlack from '../static/images/logo-black.png'
import LogoWhite from '../static/images/logo-white.png'
import { Link, withRouter } from "react-router-dom";
import {items} from '../mapper/list_menu';

class Menu extends Component {
	constructor() {
		super();
		this.state = {
			menu: false
		}
	}
	howColor() {
		return this.props.isColor;
	}
	logotype() {
		const logo = this.howColor();
		return (
			<Link to={'/main'} className={'menu-'+(this.state.size)+'-logo'}>
				<img
					src={logo === 'black' && !this.state.menu ? LogoBlack : LogoWhite}
					alt="Logo of Jungle King's"
					onClick={() => this.setState({menu: false}) }
				/>
			</Link>
		)
	}
	menuDesktop() {
		const listItem = (list) => {
			return list.map(key => {
				return key.link === 'call' ?
					<li key={key.id}><span onClick={this.props.handlerModal} className={this.howColor()}>{key.value}</span></li> :
					key.value === 'logo' ? <li key={key.id}>{this.logotype()}</li> :
					<li key={key.id}><Link to={`/${key.link}`} className={this.howColor()}>{key.value}</Link></li>
			})
		};
		return (
			<nav className="menu-desktop-nav">
				<ul>
					{listItem(items)}
				</ul>
			</nav>
		)
	}
	hasSidebar() {
		const list = (props) => {
			return props.filter(key => key.value !== 'logo').map(key => {
				return key.link === 'call' ?
					<span key={key.id} onClick={this.props.handlerModal}><li>{key.value}</li></span> :
					<Link to={`/${key.link}`} key={key.id} onClick={() => this.setState({menu: false})}>
						<li>{key.value}</li>
					</Link>
			})
		};
		return(
			<div className="wrapper-menu">
				<nav className="sidebar">
					<ul>{list(items)}</ul>
				</nav>
			</div>
		)
	}
	menuMobile() {
		return (
			<>
				<div className="menu-mobile">
					{this.logotype.call(this)}
					<button
						className={"menu-mobile-button " + (this.state.menu ? 'red' : this.howColor())}
						onClick={() => this.setState({menu: !this.state.menu})}
					>
						<span className={"menu-mobile-button--open" + (this.state.menu ? ' menu-mobile-button--close': '')} />
					</button>
				</div>

				{this.state.menu ? this.hasSidebar(items) : <></>}
			</>
		)
	}
	render() {
		return (
			<header className="menu">
				{this.props.sizePage  === 'desktop' ? this.menuDesktop() : this.props.sizePage  === 'laptop' ? this.menuDesktop() : this.menuMobile()}
			</header>
		)
	}
}

export default withRouter(Menu);
