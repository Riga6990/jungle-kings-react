import React, {Component} from 'react';
import { Route, withRouter } from "react-router-dom";
import Menu from './components/Menu.js';
// all pages
import Main from './pages/Main'
import Live from './pages/Live'
import Award from './components/live/Awards'
import Gallery from './components/live/Gallery'
import Photo from './components/live/Photography'
import Films from './components/live/Films'
import Styles from './pages/Styles'
import Teachers from './pages/Teachers'
import Review from './pages/Review'
import Schedule from './pages/Schedule'
import Contacts from './pages/Contacts'
import NotFound from './pages/NotFound'
// // footer
import RangeSlider from './components/RangeSlider'
import Modal from './components/Modal'

class App extends Component {
	constructor (props) {
		super(props);
		this.handlerModal = this.handlerModal.bind(this);
		this.state = {
			modal: false,
			sizePage: '',
			color: 'black',
			pages: ['main', 'live', 'styles', 'teachers', 'review', 'schedule', 'contacts']
		}
	}
	hasBackground(background) {
		switch (background) {
			case 'main':
				return 'main';
			case 'live':
				return 'live';
			case 'styles':
				return 'styles';
			case 'schedule':
				return 'schedule';
			case 'contacts':
				return 'contacts';
			default:
				return 'other'
		}
	}
    handlerModal () {
        this.setState((state) => {
            return state.modal = !state.modal
        })
	}
	toggleModal () {
		return this.state.modal ? (<Modal handlerModal={this.handlerModal} />) : (<></>)
	}
	isColor(props) {
		this.setState({color: ['/main', '/contacts'].includes(props === undefined ? this.props.location.pathname : props) ? 'white' : 'black'})
	}
	componentDidMount() {
		this.updateDimensions();
		this.isColor();
		this.unlisten = this.props.history.listen(({pathname}) => {
			this.isColor(pathname)
		});
		window.addEventListener('resize', this.updateDimensions.bind(this));
	}

	componentWillUnmount() {
		this.unlisten();
		window.removeEventListener('resize', this.updateDimensions.bind(this));
	}

	updateDimensions() {
		if (window.matchMedia('(max-width: 767px)').matches) {
			this.setState({sizePage: 'mobile'})
		} else if (window.matchMedia('(max-width: 1023px)').matches) {
			this.setState({sizePage: 'tablet'})
		} else if (window.matchMedia('(max-width: 1199px)').matches) {
			this.setState({sizePage: 'laptop'})
		}else {
			this.setState({sizePage: 'desktop'})
		}
	}
	pages(page) {
		return(
			<div className="App" id={this.hasBackground(page)}>
				<div className="container">

					<Menu
						handlerModal = {this.handlerModal}
						sizePage={this.state.sizePage}
						isColor={this.state.color}
					/>

					{/*<Route exact path="/main" render = { () => <Main {...this} /> } />*/}
					{/*<Route path="/live" render={ () => <Live {...this} /> } />*/}
					{/*<Route path="/live/awards" component={Award} />*/}
					{/*<Route path="/live/gallery" component={Gallery} />*/}
					{/*<Route path="/live/gallery/photo" component={Photo} />*/}
					{/*<Route path="/live/gallery/video" component={Films} />*/}
					{/*<Route path="/styles" render={ () => <Styles {...this} />} />*/}
					{/*<Route path="/teachers" render={ () => <Teachers {...this} /> } />*/}
					{/*<Route path="/review" render={ () => <Review {...this} /> } />*/}
					{/*<Route path="/schedule" render={ () => <Schedule {...this} sizePage={this.state.sizePage} />} />*/}
					{/*<Route path="/contacts" render={ () => <Contacts {...this} />} />*/}

					<RangeSlider
						sizePage={this.state.sizePage}
						isColor={this.state.color}
						links={this.state.pages}
					/>

				</div>
				{this.toggleModal()}
			</div>
		)
	}
	notFound() {
		return (<NotFound />)
	}
	render () {
		const page = this.props.location.pathname.split('/')[1],
			hasPage = this.state.pages.some(key => key === page);

		return (<>{hasPage ? this.pages(page) : this.notFound()}</>)
	}
}
export default withRouter(App);

