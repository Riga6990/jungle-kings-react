import vk from '../static/images/vk.png'
import ig from '../static/images/insta.png'
import youtube from '../static/images/youtube.png'

export const soc_network = [
	{id: 1, img: vk, link: 'https://vk.com/junglekings', alt: 'Jungle King to VK'},
	{id: 2, img: ig, link: 'https://www.instagram.com/jungle_kings/', alt: 'Jungle King to Instagram'},
	{id: 3, img: youtube, link: 'https://www.youtube.com/watch?v=QbnhSTJXg20', alt: 'Jungle King to YouTube'}
];
