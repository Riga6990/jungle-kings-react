import {photos} from "./photos";

export const olya = {
	name: 'Гулина Ольга',
	social_network: {
		vk: {
			link: 'https://vk.com/hely_v',
			photo: photos('olya', 1)
		},
		ig: {
			link: 'https://www.instagram.com/hely_v/',
			photo: photos('olya', 2)
		},
		photos: [
			photos('olya', 3),
			photos('olya', 4),
			photos('olya', 5),
			photos('olya', 6),
			photos('olya', 7),
			photos('olya', 1),
			photos('olya', 2)
		]
	},
	directions: ['vogue'],
	experience: '2002',
	mentor: '2012',
	about_self: [
		'Училась у Леры-Халва Zorra',
		'Является преподавателем ЦТР "Jungle Kings" с 2015 года',
	],
	personal_achievements: [
		'Состоит в команде "Коси-Коса"',
		'- 1 место в номинации "Best Lades Show" на фестивале GENERATION 5 г.Москва 2013 г;',
		'- 1 место в номинации "Best Show" на Russian Roulette VOGUE Ball г. Н.Новгород 2014г;',
		'- 1 место в номинации "Best Dance Show" на фестивале Show Me Your Show г.Москва 2015г;',
		'- 2 место в номинации "Lady Styles Show (команды)" на 6ом Межрегиональном Танцевальном Фестиваль "Dancewave" г.Рязань 2015г.;',
		'- финалист номинации VOGUE на Zelenograd selection on "TEAMKA-2014";',
		'- победитель в номинации "VOGUE Performance" на первом "VOGUE Ball" в г.Тверь,2014г.;',
		'- участник Russian Label Vogue Ball 2015 - финалист номинации VOGUE на selection on "TEAMKA-2015" г.Рязань.'
	],
	master_class: [
		'- Карины Ninja-Zorra (Russia)',
		'- Smetana Ninja (Russia)',
		'- Alan UltraOmni (Russia)',
		'- Anik Ninja-Zorra (Russia)',
		'- Dashaun Wesley (The Father of House Of Xclusive Lanvin, USA)',
		'- Nagato Ninja (Italy)',
		'- Marina UltraOmni (Holland)'
	]
};
