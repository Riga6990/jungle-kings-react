import {photos} from "./photos";

export const jenya = {
	name: 'Евгений Иванов',
	social_network: {
		vk: {
			link: 'https://vk.com/trevgeny',
			photo: photos('jenya', 1)
		},
		ig: {
			link: 'https://www.instagram.com/coach_ivanov_evgeny/',
			photo: photos('jenya', 2)
		},
		photos: [
			photos('jenya', 3),
			photos('jenya', 4),
			photos('jenya', 5),
			photos('jenya', 6),
			photos('jenya', 7),
			photos('jenya', 1),
			photos('jenya', 2)
		]
	},
	directions: ['krump'],
	experience: '2010',
	mentor: '2013',
	about_self: [
		'В настоящий момент является одним из создателей и преподавателем по KRUMP в Центре Танцевального развития «Jungle Kings» в городе Твери.',
		'Должностные обязанности и результаты:',
		'- проведение групповых и индивидуальных занятий в разных возрастных группах;',
		'- постановка шоу-номеров, подготовка к соревнованиям, ведение молодежных мероприятий.',
		'Является одним из организаторов Фестиваля уличной культуры «Status 69»,мастер-классов от ведущих танцоров.'
	],
	personal_achievements: ['Финалист Тверских соревнований, а так же участник СНГ соревнований таких как BUCK SEASON 1 и 2, KRUMP BULLS, RESPECT MY TALENT, M357 BATTLEZONE и др.'],
	master_class: [
		'От иностранных преподавателей:',
		'- Grichka(KRUMP); ',
		'- Baby Eyez aka Beast (KRUMP);',
		'- Twin Tight Eyez aka Bdash (KRUMP);',
		'- Knucklehead (KRUMP);',
		'- Jr Style Ripper (KRUMP).',
		'От российских и представителей СНГ:',
		'- Slam aka Lil Stagekilla(KRUMP); ',
		'- Girl Tight Eyez(KRUMP); ',
		'- J Slam(KRUMP);',
		'- Ugly Fate(KRUMP);',
		'- 88 stones(KRUMP);',
		'- Girl bdash(KRUMP); ',
		'- Queen HatPlayerl(KRUMP);',
		'- Lil Slam(KRUMP) Tension(KRUMP);',
		'- Виктор Квятковский(Popping);',
		'- Рамон(Popping);',
		'- Руслан twist(Popping);',
		'- Инга Фоминых(reggaeton).'
	]
};
