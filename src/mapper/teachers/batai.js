import {photos} from "./photos";

export const batai = {
	name: 'Андрей Батаев',
	social_network: {
		vk: {
			link: 'https://vk.com/andreibatai/',
			photo: photos('batai', 1),
			translateY: -45
		},
		ig: {
			link: 'https://www.instagram.com/andreibatai/',
			photo: photos('batai', 2)
		},
		photos: [
			photos('batai', 3),
			photos('batai', 4),
			photos('batai', 5),
			photos('batai', 6),
			photos('batai', 7),
			photos('batai', 1),
			photos('batai', 2)
		]
	},
	directions: ['hip-hop', 'popping'],
	experience: '2009',
	mentor: '2010',
	about_self: [
		'В настоящий момент является одним из создателей зам. директором и преподавателем по Hip-Hop , Popping в Центре Танцевального развития «Jungle Kings» в городе Твери.',
		'Должностные обязанности и результаты:',
		'- проведение групповых и индивидуальных занятий в разных возрастных группах;',
		'- постановка шоу-номеров, подготовка к соревнованиям, ведение молодежных мероприятий;',
		'Также является одним из организаторов Фестиваля уличной культуры «Status 69».'
	],
	personal_achievements: [
		'2015 г.:',
		'- отбор на Teamka 2015 1ое место г. Рязань;',
		'- топ 8 лучших на международном фестивале RESPECT MY TALENT г. Санкт Петербург.',
		'2014 г.:',
		'- кубок дня города битва педагогов 1ое место;',
		'- Йолка 2ое место hip-hop pro г. Кострома;',
		'- Реакция 1ое место hip-hop pro горки 10;',
		'- Xxl 2ое место hip-hop pro Сергеев пассад;',
		'- Respect my talent top 32 dancers;',
		'- Teamka Zelenograd hip-hop 2 место.',
		'2013 г.:',
		'- Festos street show 1ое место москва;',
		'- Dance it is me 2 ое место hip-hop pro в. Новгород;',
		'- Dance wave 1ое место hip-hop pro Рязань;',
		'- Festos 2 ое место hip-hop pro москва;',
		'- Generation 5 best show 1 место jungle kings.',
		'2012 г.:',
		'- Dance wave 3 место jungle kings г. Рязань;',
		'- кубок дня города 1 место hip-hop pro;',
		'- Ты звезда танцпола Дуэт 1 место;',
		'- Dance wave 2 место hip-hop pro;',
		'- Ты звезда танцпола 2 место hip-hop pro;',
		'- Level up hip-hop 1 место.',
		'2011 г.:',
		'- Hip-hop on the stage jungle kings 1ое место Москва;',
		'- кубок дня города hip-hop pro 2 место.',
		'2010 г.:',
		'- кубок дня города hip-hop Pro 2 место;',
		'- селигер хип-хоп Баттл 1 место.',
		'Лучшие достижения учеников:',
		'- 1ое место на международном фестивале RESPECT MY TALENT хип-хоп 2х2 дети 2012г;',
		'- 1ое место на международном фестивале RESPECT MY TALENT хип-хоп 2х2 дети 2013г;',
		'- 1ое место на фестивале Person 2 Person хип-хоп 1х1 2013г город Минск, Белорусия.;',
		'- 1ое место на фестивале Dance is me хип-хоп 1х1 город Великий Новгород 2014г;',
		'- 1ое место на фестивале Dance Wave хип-хоп 1х1 город Рязань 2014г.;',
		'- 1ое место на фестивале Coliseum battle хип-хоп 1х1 город Москва 2015г.'
	],
	master_class: [
		'Зарубежные:',
		'- Meech (Франция);',
		'- Fabrice (Франция);',
		'- Marta, Jummy ydat, Hiro (Япония);',
		'- Iron make (California USA);',
		'- Budda strage usa popping (Pittsburg USA);',
		'а также брал мастер-классы у Jroch, Zulu, Maximus, Tolu, Batalla.',
		'Русские:',
		'- курс Dance Music School;',
		'- экспериментальный лагерь уличной культуры Jacks Garret;',
		'- танцевальный лагерь Next Level;',
		'- Yalta Summer Jam;',
		'а также брал мастер-классы у таких, как Барышев Евгения, Егор Соколов, Damien, Дельцова Анна, Чаусов Евгений, Ковтун Анна, Кристи, Кадет, Сидоров Артем, Виктор Квятковский, Рамон, Руслан, Twist, Серж, Кумель, Заворицкий Боря и многие другие.']
};
