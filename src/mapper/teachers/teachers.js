import { batai } from './batai'
import { halva } from './halva'
import { sasha } from './sasha'
import { masha } from './masha'
import { nastya } from './nastya'
import { natasha } from './natasha'
import { jenya } from './jenya'
import { olya } from './olya'

export const teachers = [
	{teacher_id: 'batai', info: batai},
	{teacher_id: 'halva', info: halva},
	{teacher_id: 'sasha', info: sasha},
	{teacher_id: 'masha', info: masha},
	{teacher_id: 'nastya', info: nastya},
	{teacher_id: 'natasha', info: natasha},
	{teacher_id: 'jenya', info: jenya},
	{teacher_id: 'olya', info: olya}
];
