import {photos} from "./photos";
export const nastya = {
	name: 'Настя BONDY',
	social_network: {
		vk: {
			link: 'https://vk.com/nastenalashyk',
			photo: photos('bondy', 1)
		},
		ig: {
			link: 'http://instagram.com/nastya_bondy',
			photo: photos('bondy', 2)
		},
		photos: [
			photos('bondy', 3),
			photos('bondy', 4),
			photos('bondy', 5),
			photos('bondy', 6),
			photos('bondy', 7),
			photos('bondy', 1),
			photos('bondy', 2)
		]
	},
	directions: [ 'house', 'tribal-fusion'],
	experience: '2008',
	mentor: '2009',
	about_self: [
		'В настоящий момент является одним из создателей и преподавателем по House dance и tribal fusion belly dance в Центре Танцевального Развития "Jungle Kings" в городе Твери.',
		'Должностные обязанности и результаты:',
		'- постановка групповых номеров;',
		'- обучение основам танцевальных стилей (House Dance; Tribal Fusion Belly Dance);',
		'- подготовка к соревнованиям;',
		'- индивидуальное обучение;',
		'- проведение досуговых мероприятий, направленных на личностное развитие учеников.',
		'Кроме того является основателем и участником двух команд : Houseband (house dance) и "Cowry" (tribal fusion belly Dance ).',
		'Являлись участниками таких фестивалей как dance wave (г Рязань ), Магия востока (г.Тверь ).'
	],
	personal_achievements: [
		'Является участником и финалистом многих танцевальных батлов, таких как:',
		'- Green battle в г. Санкт-Петербурге;',
		'- Mir Москва;',
		'- Juste Debout г. Санкт-Петербург;',
		'- Plur battle г. Москва;',
		'- What is house г. Минск (Белоруссия);',
		'- Fun box г. Москва.',
		'- участие в crazy week with mij г. Москва;',
		'Так же была в Summer Dance Camp SDC в Чехии.'
	],
	master_class: [
		'- Calif (House);',
		'- Rabah (House);',
		'- EJOE (House);',
		'- Physs (Hip-hop);',
		'- Ukay (Hip-hop);',
		'- Niki (Hip-hop);',
		'- Hero (House);',
		'- Снежана (House);',
		'- Алеся (House);',
		'- Банзай (House);',
		'- Полина Шандарина (Tribal Fusion);',
		'- Martina (USA ) (Tribal Fusion);',
		'- Moria Chappel (Tribal Fusion).'
	]
};
