import {photos} from "./photos";

export const masha = {
	name: 'Маша',
	social_network: {
		vk: {
			link: 'https://vk.com/',
			photo: photos('without_photo', 'nophoto')
		},
		ig: {
			link: 'https://www.instagram.com/',
			photo: photos('without_photo', 'nophoto')
		},
		photos: [
			photos('without_photo', 'nophoto')
		]
	},
	directions: ['vogue'],
	experience: '',
	mentor: '',
	about_self: ['В ближайшее время мы добавим информацию.'],
	personal_achievements: ['В ближайшее время мы добавим информацию.'],
	master_class: ['В ближайшее время мы добавим информацию.']
};
