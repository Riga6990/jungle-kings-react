import {photos} from "./photos";

export const natasha = {
	name: 'Наташа Понедельник',
	social_network: {
		vk: {
			link: 'https://vk.com/nataschaponedelnic',
			photo: photos('ponedelnik', 1)
		},
		ig: {
			link: 'http://instagram.com/natascha_ponedelnic',
			photo: photos('ponedelnik', 2)
		},
		photos: [
			photos('ponedelnik', 3),
			photos('ponedelnik', 4),
			photos('ponedelnik', 5),
			photos('ponedelnik', 6),
			photos('ponedelnik', 7),
			photos('ponedelnik', 1),
			photos('ponedelnik', 2)
		]
	},
	directions: ['contemporary', 'strip'],
	experience: '2010',
	mentor: '2012',
	about_self: [
		'В настоящий момент является одним из преподавателей по Contemporary dance и Strip в Центре Танцевального развития «Jungle Kings» в городе Твери.',
		'Должностные обязанности и результаты:',
		'- проведение групповых и индивидуальных занятий в разных возрастных группах;',
		'- постановка шоу-номеров,мини-спектаклей, а так же подготовка к соревнованиям.'
	],
	personal_achievements: [
		'- COSMIC DANCE BATTLE - Москва - победитель, в составе команды ЧИПЕЦ, победител соло;',
		'- Live dancing vol. 2 - Москва - победитель, в составе команды ЧИПЕЦ',
		'- Frame UP - Москва - победитель, в составе команды ЧИПЕЦ Dance Wave IV;',
		'- V - Рязань - победитель, в составе команды ЧИПЕЦ в номинации Lady Dance;',
		'- победитель в составе комнды "Дети понедельника" в номинации Contemporary Dance Всероссийский фестиваль "РСВ" - Ульяновск;',
		'- Тольятти - Лаурят 2 степени в составе команды "Страна глухих" В номинации "Современный танец. Малые формы".',
		'В период с 2012-2014 годах было создано 2 команды: "Страна глухих" и "Дети Понедельника", которые добились хороших результатов.',
		'"Страна глухих":',
		'Лауряты 2 степени на Всероссийском Фестивале РСВ-2013 в номинации "Современный танец.Малые формы", г. Ульяновск;',
		'"Дети Понедельника":',
		'"Dance Wave V" 2014 - 1 место в номинации Contemporary Show Также является одним из участников команды ЧИПЕЦ (Strip).'
	],
	master_class: [
		'- "BDDC" ( Бородицкий Денис, Ярцев Антон, Херолянц Марина);',
		'- г. Москва - Contemporary Dance "Minoga Dance Company" (Манылов Илья);',
		'- г. Екатеринбург - Contemporary Dance (Нетесова Татьяна);',
		'- г. Москва,г. Екатеринбург - Modern Dance (Юлия Шевченко);',
		'- г. Москва - Contemporary Dance (Екатерина Рыжакова); ',
		'- г. Москва - Contemporary Dance'
	]
};
