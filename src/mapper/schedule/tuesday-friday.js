import { breakDance } from './teachers/break-dance';
import { vogue } from './teachers/vogue';
import { hip_hop } from './teachers/hip_hop';
import { kontemporary } from './teachers/kontemporary';
import { jkids } from './teachers/jkids';
import { strip } from './teachers/strip';
import { vogue_olya } from './teachers/vogue_olya';

export const tuesdayFriday = [
	{
		hour: '13:00-15:00',
		one_hall: {},
		two_hall: {}
	},
	{
		hour: '16:00-17:00',
		one_hall: { ...breakDance },
		two_hall: { ...vogue, group: ['Новички 9-12'] }
	},
	{
		hour: '17:00-18:00',
		one_hall: { ...hip_hop, group: ['PRO'] },
		two_hall: { ...kontemporary, group: ['KIDS 7-10'] }
	},
	{
		hour: '18:00-19:00',
		one_hall: { ...hip_hop, group: ['PRO'] },
		two_hall: { ...vogue, group: ['Новички',  'Взрослые'] }
	},
	{
		hour: '19:00-20:00',
		one_hall: { ...jkids },
		two_hall: { ...kontemporary, group: ['КОМАНДА'] }
	},
	{
		hour: '20:00-21:00',
		one_hall: { ...jkids },
		two_hall: { ...vogue_olya, group: ['PRO'] }
	},
	{
		hour: '21:00-22:00',
		one_hall: { ...strip },
		two_hall: {}
	}
];
