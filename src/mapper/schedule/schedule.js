import { mondayThursday } from './mondey-thursday'
import { tuesdayFriday } from './tuesday-friday'
import { wednesdaySaturday } from './wednesday-saturday'

export const schedule = [
    {day: 'Понедельник/Четверг', hours: mondayThursday},
    {day: 'Вторник/Пятница', hours: tuesdayFriday},
	{day: 'Среда/Суббота', hours: wednesdaySaturday}
];

// $green: #28e899;
// $pink: #f2b4ff;
// $orange: #e87e28;
// $blue: #28cbe8;
// $purple: #b8c7ff;
// $violet: #9028e8;
