import { tribal_fussion } from './teachers/tribal_fussion';
import { krumb } from './teachers/krumb';
import { vogue_olya } from './teachers/vogue_olya';

export const wednesdaySaturday = [
	{
		hour: '13:00-15:00',
		one_hall: {},
		two_hall: { ...tribal_fussion }
	},
	{
		hour: '16:00-17:00',
		one_hall: {},
		two_hall: {}
	},
	{
		hour: '17:00-18:00',
		one_hall: {},
		two_hall: { ...krumb, group: ['ДЕТИ'] }
	},
	{
		hour: '18:00-19:00',
		one_hall: {},
		two_hall: { ...krumb }
	},
	{
		hour: '19:00-20:00',
		one_hall: { ...vogue_olya, group: ['Новички'] },
		two_hall: {}
	},
	{
		hour: '20:00-21:00',
		one_hall: {},
		two_hall: {}
	},
	{
		hour: '21:00-22:00',
		one_hall: {},
		two_hall: {}
	}
];
