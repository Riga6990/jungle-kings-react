import { vogue } from './teachers/vogue';
import { vogue_nastya_masha } from './teachers/vogue_nastya_masha';
import { kontemporary } from './teachers/kontemporary';
import { hip_hop } from './teachers/hip_hop';
import { strip } from './teachers/strip';
import { popping } from './teachers/popping';

export const mondayThursday = [
	{
		hour: '13:00-15:00',
		one_hall: {},
		two_hall: {}
	},
	{
		hour: '16:00-17:00',
		one_hall: {},
		two_hall: {}
	},
	{
		hour: '17:00-18:00',
		one_hall: { ...vogue, group: ['PRO'] },
		two_hall: { ...vogue_nastya_masha, group: ['Новички',  'Взрослые'] }
	},
	{
		hour: '18:00-19:00',
		one_hall: { ...vogue, group: ['PRO'] },
		two_hall: { ...kontemporary, group: ['Новички'] }
	},
	{
		hour: '19:00-20:00',
		one_hall: { ...hip_hop, group: ['ДЕТИ', 'Новички'] },
		two_hall: { ...kontemporary, group: ['PRO'] }
	},
	{
		hour: '20:00-21:00',
		one_hall: { ...strip },
		two_hall: { ...hip_hop, group: ['Новички',  'Взрослые'] }
	},
	{
		hour: '21:00-22:00',
		one_hall: { ...strip },
		two_hall: { ...popping }
	}
];
