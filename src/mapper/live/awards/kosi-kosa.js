export const kosi_kosa = [
	{
		year: '2015',
		events: [
			'1 место на SMYS Champ Spring, 2015 г. (г. Москва) в номинации «Best Dance Show»',
			'2 место на Dancewave – 6-й Межрегиональный Танцевальный Фестиваль, 2015 г. (г. Рязань) в номинации «Lady Styles Show (команды)»'
		]
	},
	{
		year: '2014',
		events: [
			'1 место на «Russian Roulette» Vogue Ball, 2014 г. (г. Н.Новгород), в номинации «Best show»'
		]
	},
	{
		year: '2013',
		events: [
			'1 место на «GENERATION 5», 2013 г. (г. Москва) в номинации «Best Ladies Show»',
			'Финалисты на Summer Vogue Camp, г. 2013 г. и Christmass Vogue Ball and Camp, 2014 г. в номинации «Best show»'
		]
	},
	{
		year: '2012',
		events: [
			'2 место на Dancewave - 3-й Межрегиональный Танцевальный Фестиваль, 2012 г. (г. Рязань) в номинации «Ladies Styles Show»'
		]
	}
];
