export const other = [
	{
		year: '2016',
		events: [
			'ФЕСТИВАЛЬ «НОГИВРУКИ», 2016 г. (г. Ярославль), в номинациях «HIP-HOP до 12 лет», «HIP-HOP до 16 лет»'
		]
	},
	{
		year: '2015',
		events: [
			'TRIBLE FUSION BATTLE, 2015 г. (г. Москва), в номинации «Профи»',
			'LUNADANCE FEST (отбор на TEAMка), 2015 г. (г. Рязань), в номинации «HIP-HOP pro 1х1»',
			'Танцевальный чемпионат «КУБОК БК», 2015 г. (г. Москва), в номинации «HIP-HOP kids» FUN BOX, 2015 г. (г. Москва), в номинации «HIP-HOP kids»'
		]
	},
	{
		year: '2014',
		events: [
			'ОТКРЫТЫЙ ФЕСТИВАЛЬ УЛИЧНОЙ КУЛЬТУРЫ XXL - HIP HOP LEAGUE, 2014 г. (г. Сергиев Посад), в номинациях «HIP-HOP kids», «HIP-HOP pro»',
			'ФЕСТИВАЛЬ HIP-HOP КУЛЬТУРЫ «ЙОЛКА», 2014 г. (г. Кострома), в номинациях «HIP-HOP beginners», «HIP-HOP pro»',
			'КУБОК ДНЯ ГОРОДА ТВЕРИ, 2014 г. (г.Тверь), в номинациях «HIP-HOP дети», «HIP-HOP новички», «All Styles»',
			'ФЕСТИВАЛЬ «ГОРКИ 10», 2014 г. (г. Москва), в номинациях «HIP-HOP juniors», «HIP-HOP pro»'
		]
	},
	{
		year: '2013',
		events: [
			'RESPECT MY TALENT, 2013 г. (г.Санкт-Петербург), в номинации «HIP-HOP 2х2 kids»',
			'URBAN FEST FESTOS, 2013 г. (г. Москва), в номинации «HIP-HOP pro»',
			'FUN BOX, 2013 г. (г. Москва), в номинациях «HIP-HOP kids», «HIP-HOP beginners»',
			'BATTLE & JAM by Playa Band, 2013 г. (г. Москва), в номинации «HIP-HOP 1X1 beginners»',
			'BATTLE «DANCE IT\'S ME», 2013 г. (г. Великий Новгород), в номинациях «HIP-HOP beginners», «HIP-HOP pro»'
		]
	},
];
