export const chipec = [
	{
		year: '2016',
		events: [
			'1 место на SMYS Champ Summer, 2016 г. (г. Москва) в номинации «Best Ladies Show»',
			'3 место на «Show magazine», 2016 г. (г. Санкт-Петербург) в номинации «Dance show choreo»'
		]
	},
	{
		year: '2015',
		events: [
			'1 место на SMYS Champ Spring, 2015 г. (г. Москва) в номинации «Best Ladies Show»',
			'1 место на фестивале FRANZY, 2015 г. (г. Ярославль) в номинации «STRIP-DANCE (Команды)»',
			'1 место на Dancewave - 6-й Межрегиональный Танцевальный Фестиваль, 2015 г. (г. Рязань) в номинации «Lady Styles Show»'
		]
	},
	{
		year: '2014',
		events: [
			'1 место на Dancewave – 5-й Межрегиональный Танцевальный Фестиваль, 2014 г. (г. Рязань) в номинации «Lady Styles Show»',
			'2 место фестивале FRAME UP Перезагрузка, 2014 г. (г. Москва) в номинации «BEST EROTIC SHOW TEAM»'
		]
	},
	{
		year: '2013',
		events: [
			'1 место на Dancewave - 4-й Межрегиональный Танцевальный Фестиваль, 2013 г. (г. Рязань) в номинации «Lady Styles Show»',
			'1 место на Life Dancing vol.2. 2013 г. (г. Москва) в номинации «Стрип Группа Про»',
			'2 место на COSMIC DANCE BATTLE, 2013 г. (г. Москва) в номинации «STRIP GROUP PROF'
		]
	},
	{
		year: '2012',
		events: [
			'1 место на фестивале FRAME UP 4, 2012 г. (г. Москва) в номинации «Стрип Пластика»',
			'1 место на COSMIC DANCE BATTLE, 2012 г. (г. Москва) в номинации «STRIP GROUP PROF»',
			'1 место на Dancewave - 3-й Межрегиональный Танцевальный Фестиваль, 2012 г. (г. Рязань) в номинации «Lady Styles Show»',
			'2 место на DANCE STAR FESTIVAL, 2012 г. (г. Москва) в номинации «Стрип Группа Про»'
		]
	}
];
