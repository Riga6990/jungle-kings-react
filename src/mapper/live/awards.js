import { chipec } from './awards/chipec';
import { kosi_kosa } from './awards/kosi-kosa';
import { chidren_ponedelnik } from './awards/children-ponedelnik';
import { strana_gluhih } from './awards/strana-gluhih';
import { other } from './awards/other';

export const awards = [
	{ direction: '«Чипец»', details: chipec },
	{ direction: '«Страна глухих»', details: strana_gluhih },
	{ direction: '«Дети понедельника»', details: kosi_kosa },
	{ direction: '«Коси Коса»', details: chidren_ponedelnik },
	{ direction: 'Мы гордимся тем, что наши преподаватели и ученики являются победителями и финалистами таких соревнований, как', details: other }
];
