function importAll(photos) {
	return photos.keys().map(photos);
}

export const images = importAll(require.context('../../static/photo/live/', false, /\.(png|jpe?g|svg)$/));
