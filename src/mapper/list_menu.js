export const items = [
	{ id: 1, value: 'Главная', link: 'main' },
	{ id: 2, value: 'Жизнь JK', link: 'live' },
	{ id: 3, value: 'Стили', link: 'styles' },
	{ id: 4, value: 'Преподаватели', link: 'teachers' },
	{ id: 5, value: 'logo', link: '' },
	{ id: 6, value: 'Отзывы', link: 'review' },
	{ id: 7, value: 'Расписание', link: 'schedule' },
	{ id: 8, value: 'Запись на занятие', link: 'call' },
	{ id: 9, value: 'Контакты', link: 'contacts' }
];
